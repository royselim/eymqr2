import * as React from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import styles from '../styles';
import {WebView} from 'react-native-webview';

export default class UrlView extends React.Component {
    render(){
		const {route} = this.props;
				
		var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
		var regex = new RegExp(expression);

		const url = route ? route.params.url : 'https://www.google.com';
        return (url.match(regex) ? <WebView source={{uri: url}} />:<View><Text>Data is not a valid URL, go back and scan again.</Text></View>)
    }
}