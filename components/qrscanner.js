import React, { Component } from 'react';
import {Alert} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';

export default class QRScanner extends Component {
	constructor(props){
		super(props);
		this.scanRef = React.createRef();
		this.unsubscribe = undefined;
	}

	componentDidMount(){
		this.unsubscribe = this.props.navigation.addListener('focus', () => {
			this.scanRef.current.reactivate();
		})
	}

	componentWillUnmount(){
		this.unsubscribe();
	}

	onSuccess = e => {
		this.props.navigation.navigate('UrlView', {
			url: e.data
		})
	};

  	render() {
		return (
			<QRCodeScanner
				ref={this.scanRef}
				onRead={this.onSuccess}
				// flashMode={QRCodeScanner.Constants.FlashMode.torch}
			/>
		);
	}
}