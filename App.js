'use strict';

import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import QRScanner from './components/qrscanner';
import UrlView from './components/urlview';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default class App extends React.Component {
	render(){
		return (
			<NavigationContainer>
				<Stack.Navigator initialRouteName="QRScanner">
					<Stack.Screen name="QRScanner" component={QRScanner} />
					<Stack.Screen name="UrlView" component={UrlView} />
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
}